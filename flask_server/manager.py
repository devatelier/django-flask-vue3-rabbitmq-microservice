# this file is obsolete
from flask_server import app, db
from flask_migrate import Migrate
from flask_script import Manager

migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db')

if __name__ == '__main__':
    manager.run()