
from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import UniqueConstraint
from flask_migrate import Migrate

import os

# flask manager is not required
# just run in container (docker-compose exec backend bash):
# flask db init
# flask db migrate



app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = 'postgresql://{db_user}:{db_password}@db-postgres/flask_db'.format(db_user=os.environ.get("DB_USER", "admin"),
                                    db_password=os.environ.get("DB_USER", "DB_PASSWORD"))
CORS()

db = SQLAlchemy(app)
migrate = Migrate(app,db)


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=False)
    title = db.Column(db.String(200))
    image = db.Column(db.String(200))

class ProductUser(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    product_id = db.Column(db.Integer)

    UniqueConstraint('user_id', 'product_id', name='user_product_unique')

@app.route('/')
def index():
    return 'Hello'

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

